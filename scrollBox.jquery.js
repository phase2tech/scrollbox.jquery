(function insertscrollBox($) {
  $.fn.scrollBox = function scrollBoxConstructor(options) {
    // Exit early if we have not any there in this.
    if (this.length == 0) { return; }

    const sb = {};
    sb.$container = this;
    sb.options = $.extend({}, $.fn.scrollBox.defaults, options);

    // define methods
    sb.refresh = $.fn.scrollBox.refresh;
    sb.move = $.fn.scrollBox.move;
    sb.init = $.fn.scrollBox.init;
    sb.enable = $.fn.scrollBox.enable;
    sb.remove = $.fn.scrollBox.remove;

    // get us started!
    sb.init();
    sb.enable();
    this.sbo = sb;

    return sb;
  };
  $.fn.scrollBox.defaults = {
    itemIdentifier: 'h2',
    moreItem: '<a href="#">More</a>',
    lessItem: '<a href="#">Less</a>',
    placement: 'side-by-side',
    speed: 100,
    manageAttach: false
  };
  $.fn.scrollBox.refresh = function refresh() {
    const scrollTop = this.$container.scrollTop();
    const sb = this;
    const hide = function hide(item) {
      if (sb.options.placement === 'side-by-side') {
        item.css('visibility', 'hidden');
      }
      else {
        item.hide();
      }
    };
    const show = function show(item) {
      if (sb.options.placement === 'side-by-side') {
        item.css('visibility', 'visible');
      }
      else {
        item.show();
      }
    };
    // Any scroll at all, show 'less'
    if (scrollTop > 0) {
      show(this.$less);
    }
    // At the very top, hide 'less'
    if (scrollTop === 0) {
      hide(this.$less);
    }
    // 'More' should hide at bottom of scroll
    if (scrollTop >= this.containerScrollHeight - this.$container.height()) {
      hide(this.$more);
    }
    // 'More' should always show otherwise
    if (scrollTop < this.containerScrollHeight - this.$container.height()) {
      show(this.$more);
    }
    // change the height of the container
    let offset = 0;
    if (this.options.placement === 'side-by-side') {
      offset = this.$more.outerHeight();
    }
    else if (this.options.placement === 'top-and-bottom') {
      offset = (this.$less.is(':visible') ? this.$less.outerHeight() : 0) + (this.$more.is(':visible') ? this.$less.outerHeight() : 0);
    }
    this.$container.height(this.containerHeight - offset);
  };
  $.fn.scrollBox.move = function move(dir) {
    // Run though the items, checking each against current direction and position
    const sb = this;
    let currentItem = 0;
    const $items = $(sb.options.itemIdentifier).add(this.$bottom);
    $items.each(function itemsPositionCheck(index) {
      const topPosition = $(this).position().top;

      if (dir === 'up' && topPosition >= sb.$container.height()) {
        currentItem = index - 1;
        return false;
      }
      else if (dir === 'down' && topPosition >= -sb.$container.height()) {
        currentItem = index;
        return false;
      }
    });
    this.$container.scrollTo($items.eq(currentItem), this.options.speed, {
      onAfter: function onAfter() {
        sb.refresh();
      },
    });
  };

  $.fn.scrollBox.enable = function enable() {
    if (this.options.manageAttach) {
      if (this.options.placement === 'side-by-side') {
        this.$container.after(this.$more).after(this.$less);
      }
      else if (this.options.placement === 'top-and-bottom') {
        this.$container.before(this.$less).after(this.$more);
      }
    }
    this.$bottom.show();
    // turn off scrolling
    this.$container.css('overflow', 'hidden');
    this.refresh();
  };
  $.fn.scrollBox.init = function init() {
    // find our starting  height
    this.containerHeight = this.$container.height();
    this.containerScrollHeight = this.$container.get(0).scrollHeight;
    // set up more button
    this.$more = $(this.options.moreItem)
      .attr('aria-hidden', 'true')
      .click(() => {
        this.move('up');
      });

    // setup less button
    this.$less = $(this.options.lessItem)
      .attr('aria-hidden', 'true')
      .click(() => {
        this.move('down');
      });


    // Create bottom item and set up way point so we no we have reach rock bottom
    this.$bottom = $('<div></div>')
      // I wish i could do this append in the enable function
      .appendTo(this.$container)
      .height(this.containerHeight);
  };
  $.fn.scrollBox.remove = function remove() {
    this.$more.remove();
    this.$less.remove();
    this.$bottom.remove();
    this.$container.scrollBoxObject = undefined;
  };
}(jQuery));


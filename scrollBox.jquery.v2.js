(function scrollBoxJquery($, _) {
  $.fn.scrollBox = function scrollBoxConstructor(options) {
    // Config merge
    // Anything we want overridable - INCLUDING FUNCTIONS - should exist here
    const config = _.assignIn({
      itemIdentifier: 'h2',
      moreItem: '<a href="#">More</a>',
      lessItem: '<a href="#">Less</a>',
      acttionPlacement: 'side-by-side', //"top-bottom"
      speed: 200,
    }, options);

    // Items and "current" top item
    const $items = $(config.itemIdentifier, this);
    // Index of $items array for current, top config.itemIdentifier
    let currentItem = 0;

    // Scrollable container height
    const containerHeight = this.height();
    // Content height inside scrollable outer container
    const contentHeight = this.get(0).scrollHeight;

    // Protected scrolling function
    this.move = function move(dir) {
      // Run though the items, checking each against current direction and position
      $items.each(function itemsPositionCheck(index) {
        const topPosition = $(this).position().top;

        if (dir === 'up' && topPosition >= containerHeight) {
          currentItem = index - 1;
          return false;
        }
        else if (dir === 'down' && topPosition >= -containerHeight) {
          currentItem = index;
          return false;
        }
      });

      // The reason this entire function exists as a member of outer scope is so
      // that right here (this.scrollTo()) can refer to the DOM element.
      // Otherwise we'd have to use .bind(this) at callee
      this.scrollTo($items.eq(currentItem), config.speed);
    };

    // Buttons
    const $more = $(config.moreItem)
      .attr('aria-hidden', 'true')
      .on('click', () => {
        this.move('up');
      });

    const $less = $(config.lessItem)
      .attr('aria-hidden', 'true')
      .on('click', () => {
        this.move('down');
      })
      .hide();

    const $bottom = $("<div></div>")
      .height(containerHeight);
    // Using lodash to debounce a function that does some heavy stuff
    const heavyScrollManipulation = _.debounce(() => {
      const scrollTop = this.scrollTop();

      // Any scroll at all, show "less"
      if (scrollTop > 0) {
        $less.show();
      }
      // At the very top, hide "less"
      if (scrollTop === 0) {
        $less.hide();
      }
      // "More" should hide at bottom of scroll
      if (scrollTop >= contentHeight - containerHeight) {
        $more.hide();
      }
      // "More" should always show otherwise
      if (scrollTop < contentHeight - containerHeight) {
        $more.show();
      }
      var offset = ($less.is(":visible") ? $less.outerHeight() : 0) +
        ($more.is(":visible") ? $less.outerHeight() : 0);
      if(config.placement === 'side-by-side') {
        offset = $more.outerHeight();
      }
      this.height(containerHeight - offset);

    }, 100);

    // Apply that heavy function to scroll event knowing it fires debounced
    this.on('scroll', heavyScrollManipulation);

    // Exposed function to kill functionality
    this.remove = () => {
      $more.remove();
      $less.remove();
    };

    // Kick off all the DOM manipulation
    (this.init = () => {
      if(config.placement === 'side-by-side') {
        this.before($less).before($more);
      }
      else {
        this.before($less).after($more)
      }
      this.css('overflow', 'hidden').append($bottom);
      this.height(containerHeight - $more.outerHeight());
    })();

    return this;
  };
}(jQuery, _));

